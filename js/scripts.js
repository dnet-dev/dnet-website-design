// Shorthand for $( document ).ready()


jQuery(document).ready(function( $ ){
/**
$('.multiple-items').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3
});*/

$('.your-class').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  infinite: true,
  slidesToShow: 5,
  slidesToScroll: 1,
  asNavFor: '.your-class',
  centerMode: true,
  focusOnSelect: true
});


$('#myModal').on('show.bs.modal', function (event) {
  			$('#section-1 .scroll.bounce').css('display','none');
  			$('#myModal .scroll.bounce').fadeIn(15000).css('display','block');

	});
	$('#myModal').on('hidden.bs.modal', function (event) {
  			$('#section-1 .scroll.bounce').css('display','block');

	});

	$('#myModal #scroll.bounce').click(function(){
		$.scrollTo($('#section-2'), 1000);
		$('#myModal').modal('hide');

	});
	
});

/* this is for redpill h2 animation */
var node = document.querySelector('h2');
var text = new T(node);

function random(min, max) {
    return (Math.random() * (max - min)) + min;
}

text.chars.map(function(v, i){
    TweenMax.from(v, 2.5, {
        opacity: 0,
        x: random(-500, 500),
        y: random(-500, 500),
        z: random(-500, 500),
        scale: .1,
        delay: i * .02,
        yoyo: true
    });
});

/* this is to make youtube embed full screen */

