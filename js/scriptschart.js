jQuery.noConflict();
jQuery(document).ready(function( $ ){
	var tokenValue = 10967018.75;//jQuery("#tSold").text();
    var perCentage = 0;
    var diffPercent = 0;
    var etherValue = 1754.723//jQuery("#eCol").text();

    if(tokenValue <= 10000000){
     
      diffPercent = (tokenValue/10000000) * 100;
      perCentage = ((diffPercent/100) * (50/100)) * 100;
    }else if(tokenValue > 10000000 && tokenValue <= 30000000){
      
      diffPercent = ((tokenValue - 10000000)/20000000) * 100;
      perCentage = (((diffPercent/100) * (12.5/100)) * 100) + 50;
    }else if(tokenValue > 30000000 && tokenValue <= 80000000){
      
      diffPercent = ((tokenValue - 30000000)/50000000) * 100;
      perCentage = (((diffPercent/100) * (12.5/100)) * 100) + 62.5;
    }else if(tokenValue > 80000000 && tokenValue <= 120000000){
     
      diffPercent = ((tokenValue - 80000000)/40000000) * 100;
      perCentage = (((diffPercent/100) * (12.5/100)) * 100) + 75;
    }else if(tokenValue > 120000000 && tokenValue <= 130000000){
      
      diffPercent = ((tokenValue - 120000000)/10000000) * 100;
      perCentage = (((diffPercent/100) * (4.16/100)) * 100) + 87.5;
    }else if(tokenValue > 130000000 && tokenValue <= 140000000){
      
      diffPercent = ((tokenValue - 130000000)/10000000) * 100;
      perCentage = (((diffPercent/100) * (4.16/100)) * 100) + 91.66;
    }else{
      
      diffPercent = ((tokenValue - 140000000)/10000000) * 100;
      perCentage = (((diffPercent/100) * (4.18/100)) * 100) + 95.82;
    }
    //var 

    jQuery("#totalTokenSold").html(tokenValue+ " SPYCE");
    jQuery("#totalEthereum").html(etherValue+ " ETHER");
	
loadChart(perCentage, tokenValue);
  function loadChart(tokenPercent, tokenValue){
    
    FusionCharts.ready(function () {
        var cpuGauge = new FusionCharts({
            type: 'hlineargauge',
            renderAt: 'chart-container',
            id: 'cpu-linear-gauge',
            animation: 1,
            width: '700',
            height: '150',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "theme": "fint",
                    "caption": "Spyce Token Distribution",
					"captionFont": "Audiowide", 
					"captionFontSize": "18", 
					"captionFontColor": "#FFFFFF", 
					"captionFontBold": "1",
                    "lowerLimit": "0",
                    "upperLimit": "100",
                    "numberSuffix": "%",
                    "chartBottomMargin": "50",  
                    "valueFontSize": "11",  
                    "valueFontBold": "0",
                    "pointerBgColor": "#ff0000",
                    "showTickValues":"0",
                    "showTickMarks":"0",
					"bgAlpha": "0", 
					"borderAlpha": "0"
                },
                "colorRange": {
                    "color": [
                        {
                            "minValue": "0",
                            "maxValue": "50",
                            "code": "#00fff3"
                        }, 
                        {
                            "minValue": "50",
                            "maxValue": "62.5",
                            "code": "#00bbd1"

                        }, 
                        {
                            "minValue": "62.5",
                            "maxValue": "75",
                            "code": "#007dff"
                        }, 
                        {
                            "minValue": "75",
                            "maxValue": "87.5",
                            "code": "#1000ff"
                        }, 
                        {
                            "minValue": "87.5",
                            "maxValue": "91.66",
                            "code": "#0200b9"
                        }, 
                        {
                            "minValue": "91.66",
                            "maxValue": "95.83",
                            "code": "#26477e"
                        }, 
                        {
                            "minValue": "95.83",
                            "maxValue": "100",
                            "code": "#302B54"
                        }
                    ]
                },
                "pointers": {
                    "pointer": [
                        {
                            "showValue":"0",
                            "tooltext": tokenPercent + "%",
                            "value": tokenPercent
                        }
                    ]
                },
                "trendPoints": {
                    "point": [
                        {
                            "startValue": tokenPercent,
                            "endValue": tokenPercent,
                            "displayValue": tokenValue + " dnet",
                            "color": "#ffffff",
                            "dashed": "0",
                            "thickness": "2"
                        }
                    ]
                }
            },
            events:{
                'renderComplete': function(eventObj, args) {
                    $(".raphael-group-3-creditgroup").css("display","none");
					$("#raphael-paper-0").css("background-color","");
                }
            }
        })
        .render();
    });
  }

// tell the embed parent frame the height of the content
  if (window.parent && window.parent.parent){
    window.parent.parent.postMessage(["resultsFrame", {
      height: document.body.getBoundingClientRect().height,
      slug: "None"
    }], "*")
  }

 });